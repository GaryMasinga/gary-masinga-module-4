import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DataMate"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 30,
            ),
            // From out theme we defined the body text2's textSize, color and a font weight
            const Text("This text has been styles on the theme"),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // The color of buttons come from the theme's secondary color which we have provided as purple
                    FloatingActionButton(
                      onPressed: () {},
                      child: const Icon(Icons.add),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
