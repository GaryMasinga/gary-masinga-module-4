import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:sa_university_prospectus/pages/home.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: AnimatedSplashScreen(
        splash: const Center(
          child: Image(image: AssetImage("img/logo.png")),
        ),
        nextScreen: const HomePage(),
        backgroundColor: const Color.fromARGB(255, 33, 33, 33),
        splashTransition: SplashTransition.scaleTransition,
        splashIconSize: 250,
        duration: 4000,
        animationDuration: const Duration(seconds: 3),
      ),
    );
  }
}
